#include "competitionmanager.h"

CompetitionManager::CompetitionManager(QQmlApplicationEngine* engine, QObject *parent) : QObject(parent) {
    m_Engine = engine;
}

QQmlComponent* CompetitionManager::createCompetition(QString competitionName, CountingSystem* plugin){
    Competition *competiton = new Competition(m_Engine);
    competiton->setName(competitionName);
    competiton->setPlugin(plugin);
    m_Competitions.append(competiton);
    m_CompetitionsInfo.append(competiton->getCompetitionInfo());
    return competiton->getCompetitionWidget();
}

QList<QObject *> CompetitionManager::getCompetitionsInfo() {
    return m_CompetitionsInfo;
}
