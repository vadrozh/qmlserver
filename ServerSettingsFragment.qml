import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Rectangle {
    id: rect
    anchors.fill: parent
    color: "#36393E"

    Text {
        id:descText
        text: "Настройки сервера"
        color: "white"
        font.pixelSize: 22
        renderType: Text.NativeRendering
        x : rect.x + (rect.width - width) / 2
        y : rect.y + (rect.height - (height + serverIpText.height + someText.height + control.height + 40 + getX())) / 2
    }

    Text {
        id: serverInfoText
        text:"Система будет запущена на адресе "
        color:"#878889"
        font.pixelSize:14
        x : rect.x + (rect.width - width - serverIpText.width) / 2
        anchors.top: descText.bottom
        anchors.topMargin: 10
    }
    Text {
        id: serverIpText
        text: backend.ip
        color:"white"
        font.pixelSize:14
        anchors.top: serverInfoText.top
        anchors.left: serverInfoText.right
    }

    Text {
        id: someText
        text:"Пожалуйста укажите порт для подключений"
        color:"#878889"
        font.pixelSize:14
        anchors.left: serverInfoText.left
        anchors.top: serverInfoText.bottom
        anchors.topMargin: 10
    }

    TextField {
        id:control
        text: backend.port
        anchors.top: someText.bottom
        anchors.topMargin: 20
        x: rect.x + (rect.width - width + getY()) / 2
        color: "#ffffff"
        font.pixelSize: 16
        validator: RegExpValidator {
            regExp: /[0-9]+/
        }

        background:
            Rectangle {
            anchors.fill: parent
               implicitHeight: 40
               implicitWidth: 200
               color: "#303338"
             CustomBorder {
                   id: rectBorder
                   commonBorder: true
                   borderRadius: 3
                   borderCurrentColor: "#6F6F6F"
                   borderDefaultColor: checkBorders(control.hovered, control.focus)
                   borderHoveredColor: "#040405"
                   borderFocusedColor: "#3EE2CC"
               }
            }
    }

    function getX(){
        if(rect.height % 2 == 0) return 1
        else return 0
    }
    function getY(){
        if(rect.width % 2 == 0) return 0
        else return 1
    }

}
