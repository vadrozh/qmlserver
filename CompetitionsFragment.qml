import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    id:rect
    anchors.fill: parent
    color:"transparent"
    Rectangle {
        id: menuRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: 240
        color: "#2F3136"

        Rectangle {
            id: rectHead
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            color: "#2F3136"
            height: 50
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 10
                font.pixelSize: 22
                color: "#ffffff"
                text: "Соревнования"
            }

            CustomBorder{
                commonBorder: false
                borderCurrentColor: "#222427"
                borderDefaultColor: "#222427"
                topBorderWidth: 0
                leftBorderWidth: 0
                rightBorderWidth: 1
                bottomBorderWidth: 1
            }
        }
        Label {
            id:iconCompetition
            anchors.top: rectHead.bottom
            anchors.left: parent.left
            anchors.leftMargin: 8

            anchors.topMargin: 20
            text: qsTr("\uf0dd")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        Label {
            anchors.top: rectHead.bottom
            anchors.left: iconCompetition.right
            anchors.leftMargin: 4
            anchors.topMargin: 20
            text: qsTr("Активные соревнования")
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        ListView{
            id:list
            anchors.top: iconCompetition.bottom
            anchors.topMargin: 20
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin:5
            anchors.right: parent.right
            anchors.rightMargin: 5
            model: backend.competitionList
            spacing: 2
            delegate: RHListItem {
                selected: model.modelData.selected
                itemText: model.modelData.name
                onClicked: {
                 //  model.modelData.selected = true
                  //  backend.openCompetitionWidget(itemText)
                //    loader.setSource(backend.openCompetitionWidget(itemText))
                    backend.openPluginComponent(itemText)
                }
            }
         //   highlight:
        }
    }
    Rectangle{
        id:pluginPort
        anchors.left: menuRect.right
        anchors.leftMargin: 1
        anchors.top: menuRect.top
        anchors.right: parent.right
        color: "#2F3136"
        height: 50
        Label{
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pixelSize: 22
            color: "#ffffff"
            text: backend.currentPort
        }

        CustomBorder{
            commonBorder: false
            borderCurrentColor: "#222427"
            borderDefaultColor: "#222427"
            topBorderWidth: 0
            leftBorderWidth: 0
            rightBorderWidth: 0
            bottomBorderWidth: 1
        }
    }

    Loader {
        id:loader
        anchors.left: menuRect.right
        anchors.top: pluginPort.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
   //     source: "Widget.qml"
        sourceComponent: backend.pluginComponent
    }

}
