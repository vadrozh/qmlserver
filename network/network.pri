QT += network
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS +=  $$PWD/tcpserver.h \
    $$PWD/tcpconnection.h \
    $$PWD/tcplist.h


SOURCES += $$PWD/tcpserver.cpp \
    $$PWD/tcpconnection.cpp \
    $$PWD/tcplist.cpp

