#pragma once

#include <QObject>
#include <QDebug>
#include <QMap>
#include <QList>
#include <QPair>
#include <QTcpSocket>
#include <QThread>
#include <QReadWriteLock>

#include "tcpconnection.h"

class TcpList : public QObject
{
    Q_OBJECT
public:
    explicit                TcpList(QObject *parent = nullptr);
    virtual int             count();
    enum MessageType    {PLUGIN, CONNECTION, COMPETITION, NOMINATION, MEMBER, REFEREE};
    enum MessageAction  {NONE=-1, LOAD, TOKEN, CREATE, DELETE, BLOCK, UNBLOCK, SCORES, DISQUAL, NAME};

protected:
    QList<TcpConnection*>       m_Connections;
    QMap<int, TcpConnection*>   m_ConnectionsMap;
    void                        removeConnection(TcpConnection *connection);
    int                         createToken();
    QString                     m_PluginsName;
    void                        checkToken(int index, int token);
    QString                     createMessage(MessageType type, MessageAction action, QString value);

    const QString messageType   = "MessageType";
    const QString messageAction = "MessageAction";
    const QString messageValue  = "MessageValue";

signals:
    void                        newDataAvailable(int index, QString message);
    void                        finished();
    void                        quitting();
    void                        newConnection(int token, int index);
    void                        delConnection(int index);
    void                        clientDisconnected();

protected slots:
    void                        newMessage(int index, QString message);
    void                        disconnected();

public slots:
    void                        addPluginName(QString pluginName);
    void                        sendMessage(int index, QString data);
    void                        start();
    void                        quit();
    void                        accept(qintptr handle, TcpConnection *connection);
};
