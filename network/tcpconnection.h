#pragma once

#include <QDebug>
#include <QTcpSocket>
#include <QObject>
#include <QQueue>

class TcpConnection : public QObject
{
    Q_OBJECT
public:
    explicit                TcpConnection(QObject *parent = nullptr);
    virtual void            setSocket(QTcpSocket *socket);
    virtual void            setToken(int token);
    virtual void            setIndex(int index);
    virtual void            send(QString data);
    QTcpSocket              *getSocket();
    void                    addToQueue(QString data);
    int                     getToken();
    bool                    isConnected();
                            ~TcpConnection();
protected:
    QTcpSocket              *m_Socket;
    void                    checkMessageSize(QByteArray byteArray, QString& message);
signals:
    void                    newDataAvailable(int index, QString message);
    void                    connectionEnded();
    void                    connectionStopped();
public slots:
    virtual void            readyRead();
    virtual void            connected();
    virtual void            disconnected();
    virtual void            bytesWritten(qint64 bytes);
private:
    QQueue<QString>         m_MessageQueue;
    bool                    m_IsActive;
    int                     m_Token;
    int                     m_Index;
    int                     m_ExpectedSize;
    void                    checkQueue();
};
