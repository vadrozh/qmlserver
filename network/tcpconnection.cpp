#include "tcpconnection.h"

TcpConnection::TcpConnection(QObject *parent) : QObject(parent)
{
    m_Index = -1;
    m_ExpectedSize = 0;
    m_Token = -1;
    m_IsActive = true;
}

TcpConnection::~TcpConnection()
{
    disconnect(m_Socket, &QTcpSocket::connected, this, &TcpConnection::connected);
    disconnect(m_Socket, &QTcpSocket::bytesWritten, this, &TcpConnection::bytesWritten);
    disconnect(m_Socket, &QTcpSocket::disconnected, this, &TcpConnection::disconnected);
    disconnect(m_Socket, &QTcpSocket::readyRead, this, &TcpConnection::readyRead);
}

void TcpConnection::setSocket(QTcpSocket *socket)
{
    m_Socket = socket;
    connect(m_Socket, &QTcpSocket::connected, this, &TcpConnection::connected);
    connect(m_Socket, &QTcpSocket::bytesWritten, this, &TcpConnection::bytesWritten);
    connect(m_Socket, &QTcpSocket::disconnected, this, &TcpConnection::disconnected);
    connect(m_Socket, &QTcpSocket::readyRead, this, &TcpConnection::readyRead);
    if(!m_IsActive) {
        m_IsActive = true;
    }
}

void TcpConnection::setToken(int token)
{
    m_Token = token;
}

void TcpConnection::setIndex(int index)
{
    m_Index = index;
}

void TcpConnection::send(QString data)
{
    qDebug() << "[SEND] " << data << endl;
   // Перевод сообщения в массив байт
   QByteArray dataArray = data.toUtf8();
   // Добавление размера сообщения в массив
   dataArray.prepend(QByteArray::number(dataArray.size()), 4);
   m_Socket->write(dataArray);
   m_Socket->flush();
}

QTcpSocket *TcpConnection::getSocket()
{
//    return this;
    return m_Socket;
}

void TcpConnection::addToQueue(QString data)
{
    m_MessageQueue.append(data);
}

void TcpConnection::checkQueue()
{
    foreach (QString message, m_MessageQueue) {
        if (this->isConnected()){
            this->send(message);
            m_MessageQueue.removeFirst();
        }
    }
}

int TcpConnection::getToken()
{
    return m_Token;
}

bool TcpConnection::isConnected()
{
    return (m_Socket->state() == QAbstractSocket::ConnectedState);
}

void TcpConnection::checkMessageSize(QByteArray byteArray, QString &message)
{
    if (!m_ExpectedSize){
            QByteArray sizeArray = byteArray.left(4);
            m_ExpectedSize = sizeArray.toInt();
            byteArray.remove(0,4);
        }

        int currentSize = byteArray.size();

        // Сообщение передано полностью, больше данных нет
        if (m_ExpectedSize == currentSize){
            message.append(QString::fromUtf8(byteArray));
            m_ExpectedSize -= currentSize;

            qDebug() << "[RECEIVE] " << message << endl;
            emit newDataAvailable(m_Index, message);
            return;
        }

        // Размер сообщения больше принятого массива.
        // Сохраняем текущее сообщени и получаем новую порцию данных
        if (m_ExpectedSize > currentSize){
            message.append(QString::fromUtf8(byteArray));
            m_ExpectedSize -= currentSize;
            return;
        }

        // Пришли склеенные сообщения
        // Добавляем левую часть принятых данных к уже имеющемуся сообщению и передаем сообщение в программу
        // Очищаем сообщение,
        if (m_ExpectedSize < currentSize){
            message.append(QString::fromUtf8(byteArray.left(m_ExpectedSize)));

            qDebug() << "[RECEIVE] " << message << endl;
            emit newDataAvailable(m_Index, message);

            message.clear();

            byteArray.remove(0,m_ExpectedSize);
            m_ExpectedSize = 0;
            checkMessageSize(byteArray, message);
            return;
        }
}

void TcpConnection::readyRead()
{
    if(qobject_cast<QTcpSocket *>(QObject::sender()) != m_Socket) return;
    m_Socket->waitForBytesWritten();
    QByteArray  byteArray;
    QString     message;
    do {
        // Если нет данных для чтения, нечего тут и делать
        if(!m_Socket->bytesAvailable())
            return;

        // Запись полученных байт в массив
        byteArray = m_Socket->readAll();

        // Проверка полученных данных
        checkMessageSize(byteArray, message);
    } while (m_ExpectedSize != 0);
}

void TcpConnection::connected()
{
    if(!sender()) return;
    qDebug() << this << "Connected " << sender();
}

void TcpConnection::disconnected()
{
    if(!sender()) return;
    m_IsActive = false;
    qDebug() << this << "Disconnected " << sender();
    if(m_Socket->isOpen()){
        m_Socket->disconnect();
        m_Socket->close();
    }
    m_Socket->deleteLater();
    if(m_Token == -1) {
         emit connectionEnded();
    } else {
        emit connectionStopped();
    }
}

void TcpConnection::bytesWritten(qint64 bytes)
{
    if(!sender()) return;
    qDebug() << this << "Bytes written "<< bytes;
}
