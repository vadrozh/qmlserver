#pragma once

#include <QObject>
#include <QTcpServer>
#include <QDebug>
#include <QThread>

#include "tcpconnection.h"
#include "tcplist.h"

class TcpServer : public QTcpServer {
    Q_OBJECT
public:
    explicit                TcpServer(QObject *parent = nullptr);
    virtual bool            listen(quint16 port);
    virtual void            close();
    virtual qint64          port();
    virtual QString         getLocalIP() const;
protected:
    QThread                 *m_Thread;
    virtual void            incomingConnection(qintptr handle);
    virtual void            accept(qintptr descriptor, TcpConnection *connection);
signals:
    void                    accepting(qintptr handle, TcpConnection *connection);
    void                    finished();
    void                    newConnection(int index, int token);
    void                    clientConnected();
    void                    clientDisconnected();
public slots:
    void                    complete();
    void                    refereeConnected(int index, int token);
private:
    QString                 m_LocalIP;
    TcpList*                m_ConnectionList;
};

