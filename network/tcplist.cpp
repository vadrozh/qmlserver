#include "tcplist.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QTime>

TcpList::TcpList(QObject *parent) : QObject(parent) {

}

int TcpList::count() {
  QReadWriteLock lock;
  lock.lockForRead();
  int value = m_Connections.count();
  lock.unlock();
  return value;
}

void TcpList::removeConnection(TcpConnection *connection) {
  if(!connection) return;
  qDebug() << this << "Removing connection";
/*  if(connection->getSocket()->isOpen()){
      connection->getSocket()->disconnect();
      connection->getSocket()->close();
  }
  int connectionIndex = m_Connections.indexOf(connection);
  emit delConnection(connectionIndex);
/*  if(!m_Connections.removeOne(connection)){
       qDebug() << this  << "Failed to remove connection";
       return;
  }
  connection->getSocket()->deleteLater();*/
  delete connection;
  qDebug() << this << "Connection removed successful";
}

int TcpList::createToken() {
  srand(rand()%100000 + 112341 +114 + QTime::currentTime().toString("hhmsz").toInt());
  return rand()%100000;
}

void TcpList::addPluginName(QString pluginName) {
  m_PluginsName += pluginName;
}

void TcpList::checkToken(int index, int token) {
  if(token == -1){
      TcpConnection *connectionSender = static_cast<TcpConnection*>(sender());
      int readyToken = createToken();
      connectionSender->send(createMessage(CONNECTION, TOKEN, QString::number(readyToken)));
      connectionSender->setToken(readyToken);
      m_Connections.append(connectionSender);
      connectionSender->setIndex(m_Connections.count()-1);
      connectionSender->send(createMessage(PLUGIN, LOAD, m_PluginsName));
      emit newConnection(readyToken, m_Connections.indexOf(connectionSender));
  } else {
      foreach (TcpConnection* connection, m_Connections) {
         if(connection->getToken() == token) {
              TcpConnection *connectionSender = static_cast<TcpConnection*>(sender());
              connection->setSocket(connectionSender->getSocket());
              qDebug() << this << "Someone reconnected";
              delete connectionSender;
         }
      }
  }

}

QString TcpList::createMessage(TcpList::MessageType type, TcpList::MessageAction action, QString value) {
  QJsonObject object;
  object.insert(messageType,     type);
  object.insert(messageAction,   action);
  object.insert(messageValue,    value);

  return QString(QJsonDocument(object).toJson(QJsonDocument::Compact));
}

void TcpList::newMessage(int index, QString message) {
  QJsonObject object  = QJsonDocument::fromJson(message.toUtf8()).object();
  if(index!=-1) {
      emit newDataAvailable(index, message);
  }
  if (object.count() != 3)
      return;

  int         type    = object.value(messageType).toInt();
  int         action  = object.value(messageAction).toInt();
  QJsonValue  value   = object.value(messageValue);

  switch (type)
  {
      case CONNECTION:
          switch (action)
          {
              case TOKEN:
                  checkToken(index, value.toString().toInt());
                  break;
          }
  }
}

void TcpList::disconnected() {
  if(!sender()) return;
  qDebug() << this << "Disconnecting socket" << sender();
  TcpConnection *connection = static_cast<TcpConnection*>(sender());
  emit clientDisconnected();
  removeConnection(connection);
}

void TcpList::sendMessage(int index, QString data) {
  TcpConnection* connection = m_Connections.at(index);
  if (connection->isConnected()){
      connection->send(data);
  } else {
      connection->addToQueue(data);
  }
}

void TcpList::start() {
  qDebug() << this << "Connections started";
}

void TcpList::quit() {
  if(!sender()) return;
  qDebug() << this << "Conntections quiting";
  foreach (TcpConnection* connection, m_Connections) {
     removeConnection(connection);
  }
  emit finished();
}

void TcpList::accept(qintptr handle, TcpConnection *connection) {
  QTcpSocket *socket = new QTcpSocket(this);
  if(!socket->setSocketDescriptor(handle)){
      qDebug() << this << "Failed to accept connection " << handle;
      connection->deleteLater();
      return;
  }
  connect(connection, &TcpConnection::connectionEnded, this, &TcpList::disconnected);
  connect(connection, &TcpConnection::newDataAvailable, this, &TcpList::newMessage);
  connect(connection, &TcpConnection::connectionStopped, this, [=](){emit clientDisconnected();});
  connection->moveToThread(QThread::currentThread());
  connection->setSocket(socket);
}

