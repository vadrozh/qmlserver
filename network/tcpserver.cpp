#include <QNetworkReply>
#include <QNetworkInterface>
#include "tcpserver.h"

TcpServer::TcpServer(QObject *parent) : QTcpServer(parent) {
    qDebug() << this << "Created";
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
             m_LocalIP = address.toString();
    }
    m_ConnectionList = new TcpList();
  //  m_Thread = new QThread(this);
  //  m_Thread->start();
 //   connect(m_Thread, &QThread::started, m_ConnectionList, &TcpList::start, Qt::QueuedConnection);
    connect(this, &TcpServer::accepting, m_ConnectionList, &TcpList::accept, Qt::QueuedConnection);
//    connect(this, &TcpServer::finished, m_ConnectionList, &TcpList::finished, Qt::QueuedConnection);
    connect(m_ConnectionList, &TcpList::clientDisconnected, this, [=](){emit clientDisconnected();});
    connect(m_ConnectionList, &TcpList::newConnection, this, &TcpServer::refereeConnected);
}

bool TcpServer::listen(quint16 port) {
    if(!QTcpServer::listen(QHostAddress(m_LocalIP), port)) return false;
    m_Thread = new QThread(this);
    m_Thread->start();

    return true;
}

void TcpServer::close() {
    qDebug() << this << "Closing server";
    emit finished();
    QTcpServer::close();
}

qint64 TcpServer::port() {
    if(isListening()){
        return this->serverPort();
    } else {
        return 20000;
    }
}

QString TcpServer::getLocalIP() const {
    return m_LocalIP;
}

void TcpServer::incomingConnection(qintptr handle) {
    qDebug() << this << "Try to accept connection" << handle;
    TcpConnection *connection = new TcpConnection();
    accept(handle, connection);
}

void TcpServer::accept(qintptr descriptor, TcpConnection *connection) {
    connection->moveToThread(m_Thread);
    emit clientConnected();
    emit accepting(descriptor, connection);
}

void TcpServer::complete() {
    qDebug() << this << "Destroying thread";
//    delete m_Connections;
    m_Thread->quit();
    m_Thread->wait();
    delete m_Thread;
}

void TcpServer::refereeConnected(int index, int token){
    emit newConnection(index, token);
}
