import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    id:rect
    anchors.fill: parent
    color:"transparent"

    Rectangle {
        id: menuRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: 240
        color: "#2F3136"

        Rectangle {
            id: rectHead
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            color: "#2F3136"
            height: 50
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 10
                font.pixelSize: 22
                color: "#ffffff"
                text: "Обзор"
            }
            CustomBorder{
                commonBorder: false
                borderCurrentColor: "#222427"
                borderDefaultColor: "#222427"
                topBorderWidth: 0
                leftBorderWidth: 0
                rightBorderWidth: 0
                bottomBorderWidth: 1
            }
        }
        Label {
            id:iconInfo
            anchors.top: rectHead.bottom
            anchors.left: rectHead.left
            anchors.leftMargin: 8

            anchors.topMargin: 20
            text: qsTr("\uf0dd")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        Label {
            anchors.top: rectHead.bottom
            anchors.left: iconInfo.right
            anchors.leftMargin: 4
            anchors.topMargin: 20
            text: qsTr("Инфо")
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        RHListItem {
            id: serverItem
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.top: iconInfo.bottom
            anchors.topMargin: 20
            selected: true
            itemText: qsTr("Сервер")
        }
        Label {
            id:iconPlugins
            anchors.top: serverItem.bottom
            anchors.left: rectHead.left
            anchors.leftMargin: 8

            anchors.topMargin: 20
            text: qsTr("\uf0dd")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        Label {
            anchors.top: serverItem.bottom
            anchors.left: iconPlugins.right
            anchors.leftMargin: 4
            anchors.topMargin: 20
            text: qsTr("Системные плагины")
            verticalAlignment: Text.AlignVCenter
            color:"#878889"
            font.pixelSize: 16
            font.family: "FontAwesome"
        }
        ListView {
            anchors.top: iconPlugins.bottom
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }

    }
    Loader {
        anchors.left: menuRect.right
        anchors.top: menuRect.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        source: "ServerInfoFragment.qml"
    }
}
