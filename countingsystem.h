#pragma once
#include <QObject>
#include <QString>
#include <QQmlComponent>

class CountingSystem : public QObject{
    Q_OBJECT
public:
   // virtual                 ~CountingSystem();
    virtual QString         getName() = 0;
    virtual QString         getComponentPath() = 0;
    virtual QString         getContextName() = 0;
    virtual QObject*        getContext() = 0;

public slots:
    virtual void            refereeConnected(int index, int token) = 0;
    virtual void            readMessage(int index, QString message) = 0;
signals:
    void                    sendMessage(int receiver, QString message);
    void                    sendService(QVariant message);

};

Q_DECLARE_INTERFACE(CountingSystem, "ru.KerraMon.RefereeHelper.ServerCountingSystem/0.1")
