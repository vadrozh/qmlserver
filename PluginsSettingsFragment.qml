import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Rectangle {
    id: rect
    anchors.fill: parent
    color: "#36393E"
    Text {
        id:descText
        text: "Плагины"
        color: "white"
        font.pixelSize: 22
        renderType: Text.NativeRendering
        x : rect.x + (rect.width - width) / 2
        y : rect.y + (rect.height - (height  + someText.height + 250)) / 2
    }
    Text{
        id: someText
        text:"Выбирите плагины, которые необходимы для вашего соревнования"
        color:"#878889"
        font.pixelSize:14
        anchors.top: descText.bottom
        anchors.topMargin: 10
        x : rect.x + (rect.width - width) / 2
    }
    ListView {
        anchors.top: someText.bottom
        anchors.topMargin: 50
        anchors.bottom: rect.bottom
        anchors.bottomMargin: 40
        anchors.left: rect.left
        anchors.leftMargin: 30
        anchors.right: rect.right
        id: list
        interactive: true
        spacing: 10
        model: pluginListModel
        delegate: PluginItem {
            itemWidth: rect.width - 150
            pluginName: model.modelData.name
            checkedPlugin: model.modelData.checked
            onCheckChanged: model.modelData.checked = check
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
    }
}
