#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFontDatabase>
#include <QDebug>
#include <QQmlContext>
#include <QPluginLoader>
#include <QQmlComponent>

#include "pluginmanager.h"
#include "serverbackend.h"
#include "competition.h"
#include "countingsystem.h"

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/fonts/fontawesome-webfont.ttf") == -1)
        qWarning() << "Failed to load fontello.ttf";
    QQmlApplicationEngine engine;

    ServerBackend* backend = new ServerBackend(nullptr, &engine);
    backend->registerPluginsData();
    backend->registerCompetitionData();
    engine.rootContext()->setContextProperty("backend", backend);
  //  engine.load(QUrl(QLatin1String("qrc:/main.qml")));

  /*  QDir pluginsDir(QDir::currentPath() + "/Plugins");
    foreach(const QString& pluginName, pluginsDir.entryList(QDir::Files)) {
        qDebug() << "===============================================================================";
        qDebug() << "Found:" << pluginName;

        QString pluginDir = pluginsDir.absoluteFilePath(pluginName);
        QPluginLoader loader(pluginDir);
        if(loader.load()) {
            if(CountingSystem* myPlugin = qobject_cast<CountingSystem*>(loader.instance())){
                QString name = myPlugin->getName();
                qDebug() << name;
               // QByteArray content = myPlugin->getQml("Widget.qml");

            }
//            loader.unload();
        } else {
            qDebug() << "Failed to load: " << loader.errorString();
        }
    }*/
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
