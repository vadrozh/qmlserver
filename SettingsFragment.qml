import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    property int fragmentIndex: 0

    id:app
    visible: true
    width: 800
    height: 500
    color: "#36393E"
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        RowLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: app.height / 12 * 3
            Rectangle {
                Layout.preferredWidth: app.width / 3
                Layout.fillWidth: true
                color: "transparent"
                Rectangle{
                    id: serverIcon
                    color: "#2DA192"
                    anchors.centerIn: parent
                    radius: 18
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        text: "\uF233"
                        font.pixelSize: 16
                    }
                }
            }
            Rectangle {
                Layout.preferredWidth: app.width / 3
                Layout.fillWidth: true
                color: "transparent"
                Rectangle{
                    id: pluginIcon
                    color: "#d3d3d3"
                    anchors.centerIn: parent
                    radius: 18
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        text: "\uF12e"
                        font.pixelSize: 16
                    }
                }
            }
            Rectangle {
                id: finishRect
                Layout.preferredWidth: app.width / 3
                Layout.fillWidth: true
                color: "transparent"
                Rectangle{
                    id:finishIcon
                    color: "#d3d3d3"
                    anchors.centerIn: parent
                    radius: 18
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        text: "\uF11d"
                        font.pixelSize: 16
                    }
                }
            }
        }
        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: app.height / 12 * 6
            Layout.alignment: Qt.AlignCenter
            Loader {
                id: loader
                Layout.fillHeight: true
                Layout.fillWidth: true
                source: "ServerSettingsFragment.qml"
            }
        }

        RowLayout {
            Layout.preferredHeight: app.height / 12 * 3
            Button {
                id: nextBtn
                text: qsTr("Далее")
                font.pointSize: 12
                anchors.left: parent.left
                anchors.leftMargin: finishRect.x + finishRect.width/2 + finishIcon.width/2 - width
                contentItem: Text{
                    text: nextBtn.text
                    font: nextBtn.font
                    color: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                background: Rectangle {
                    id:backRect
                    color:"#2DA192"
                    implicitWidth: 100
                    implicitHeight: 40
                    radius: 3
                }
                onHoveredChanged: {
                    if(nextBtn.hovered){
                        backRect.color = "#32A898"
                    } else {
                       backRect.color = "#2DA192"
                    }
                }

                onPressed:{
                    backRect.color = "#42B8A8"
                }
                onReleased: {
                    backRect.color = "#2DA192"
                }

                onClicked: {
                    app.fragmentIndex +=1
                    loader.setSource(backend.getFragmentName(app.fragmentIndex))
                    serverIcon.color = "#d3d3d3"
                    pluginIcon.color = "#2DA192"
                }

            }
        }

    }
}
