#pragma once

#include <QObject>
#include <QQmlComponent>
#include <QQmlApplicationEngine>

#include "tcpserver.h"
#include "countingsystem.h"

struct competitionInfo_t : public QObject {
    explicit                competitionInfo_t(QObject *parent = nullptr);
    void                    setName(QString name);
    QString                 getName();
signals:
    void                    nameChanged();
private:
    Q_OBJECT
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)

    QString m_Name;
};

class Competition : public QObject {
    Q_OBJECT
public:
    explicit                Competition(QQmlApplicationEngine *engine = nullptr, int port = 50120, QObject *parent = nullptr);
    void                    setName(QString name);
    void                    setPlugin(QObject* plugin);
    competitionInfo_t*      getCompetitionInfo();
    QQmlComponent*          getCompetitionWidget();
private:
    QQmlComponent*          m_Component;
    CountingSystem*         m_Plugin;
    competitionInfo_t*      m_Info;
    QQmlApplicationEngine*  m_Engine;
    TcpServer*              m_Server;
signals:
    void                    refereeConnected();
    void                    refereeDisconnected();
public slots:
};

