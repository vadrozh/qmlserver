#include <QVariant>
#include "serverbackend.h"

ServerBackend::ServerBackend(QObject *parent, QQmlApplicationEngine *engine) : QObject(parent) {
    m_Port = "";
    m_CurrentCompetitionPort = "";
    m_Engine = engine;
    m_MainServer = new TcpServer();
    setIp(m_MainServer->getLocalIP());
    m_PluginManager = new PluginManager();
    m_CompetitionManager = new CompetitionManager(m_Engine);
    connect(m_CompetitionManager, &CompetitionManager::clientConnected, this, &ServerBackend::refereeConnected);
    connect(m_CompetitionManager, &CompetitionManager::clientDisconnected, this, &ServerBackend::refereeDisconnected);
    m_PluginManager->loadPlugins(QDir(QDir::currentPath() + "/Plugins"));
    m_CurrentFragmentName = "SettingsFragment.qml";
    m_RightFragmentName = "ServerMainFragment.qml";
    m_ClientsCount = "0";
    m_Clients = 0;
    m_CompetitionsCount = 0;
    m_CompetitionPort = 50100;
    m_CurrentComponent = nullptr;
}

void ServerBackend::registerPluginsData() {
    QList<QObject*> info = m_PluginManager->getPluginsInfo();
    m_Engine->rootContext()->setContextProperty("pluginListModel", QVariant::fromValue(info));
}

void ServerBackend::registerCompetitionData() {
    QList<QObject*> info = m_CompetitionManager->getCompetitionsInfo();
    m_Engine->rootContext()->setContextProperty("competitionsListModel", QVariant::fromValue(info));
}

QList<QObject *> ServerBackend::getCompetitionList(){
    return m_CompetitionManager->getCompetitionsInfo();
}

QString ServerBackend::getFragmentName(int fragmentNum) {
    if(fragmentNum == 0) {
        return "SettingsFragment.qml";
    } else if(fragmentNum == 1) {
        return "PluginsSettingsFragment.qml";
    } else if(fragmentNum == 2) {
       endInitialize();
       setFragment("MainFragment.qml");
       return "EmptyFragment.qml";
    }
}

void ServerBackend::setMainLoader(QObject *loader) {
    m_MainLoader = loader;
}

void ServerBackend::setRightFragment(QString fragmentName) {
    m_RightFragmentName = fragmentName;
    emit rightFragmentChanged();
}

void ServerBackend::endInitialize() {
    QList<QObject*> info = m_PluginManager->getCheckedCompetitionPlugins();
    m_Engine->rootContext()->setContextProperty("competitionPluginsModel", QVariant::fromValue(info));
    m_MainServer->listen(m_Port.toInt());
}

void ServerBackend::createCompetition(QString competitionName, QString pluginName) {
    QQmlComponent *component = m_CompetitionManager->createCompetition(competitionName,
                                m_PluginManager->getCompetitionPlugin(pluginName));
    setPluginComponent(component);
    m_PlugnsComponents.insert(competitionName, component);
    m_CompetitionsCount++;
    emit competitionsCountChanged();
    emit competitionListChanged();
}

void ServerBackend::openCompetitionWidget(QString name) {
    //setPluginFragment(m_CompetitionManager->getWidget(name));
    //setPluginFragment(name);
}

void ServerBackend::setPluginComponent(QQmlComponent *component) {
    m_CurrentComponent = component;
    emit pluginComponentChanged();
}

void ServerBackend::refereeConnected() {
    m_Clients++;
    m_ClientsCount = QString::number(m_Clients);
    emit clientsCountChanged();
}

void ServerBackend::refereeDisconnected() {
    m_Clients--;
    m_ClientsCount = QString::number(m_Clients);
    emit clientsCountChanged();
}

QString ServerBackend::getFragment() {
    return m_CurrentFragmentName;
}

QString ServerBackend::getCurrentPort() {
    return m_CurrentCompetitionPort;
}

QString ServerBackend::getRightFragment() {
    return m_RightFragmentName;
}

QString ServerBackend::getIp() {
    return m_Ip;

}

QString ServerBackend::getPort(){
    return m_Port;
}

QString ServerBackend::getClientsCount() {
    return QString::number(m_Clients);
}

QString ServerBackend::getCompetitionsCount() {
    return QString::number(m_CompetitionsCount);
}

/*QString ServerBackend::getPluginFragment() {
    return m_PluginFragmentName;
    //  return m_PluginFragmentName;
}*/

QQmlComponent *ServerBackend::getPluginComponent() {
    return m_CurrentComponent;
}

void ServerBackend::setCurrentPort(QString port) {
    m_CurrentCompetitionPort = port;
    emit currentPortChanged();
}

void ServerBackend::setCompetitionsCount(QString competitionsCount) {
    m_CompetitionsCount = competitionsCount.toInt();
    emit competitionsCountChanged();
}

void ServerBackend::setClientsCount(QString clientsCount) {
    m_ClientsCount = clientsCount;
    emit clientsCountChanged();
}

void ServerBackend::setPort(QString port){
    m_Port = port;
    emit portChanged();
}

void ServerBackend::setIp(QString ip){
    m_Ip = ip;
    emit ipChanged();
}

void ServerBackend::openPluginComponent(QString component) {
    setPluginComponent(m_PlugnsComponents.value(component));
}

/*void ServerBackend::setPluginFragment(QString component) {
 //   m_PluginFragmentName = name;
    m_PluginFragmentName = "Widget.qml";
    emit pluginFragmentChanged();
}*/

void ServerBackend::setFragment(QString name) {
    m_CurrentFragmentName = name;
    emit fragmentChanged();
}
