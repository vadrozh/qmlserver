import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3 

ApplicationWindow {
    id:app
    visible: true
    width: 900
    height: 700
    title: qsTr("RHServer")
    color: "#36393E"

    Loader {
        id:loader
        objectName: "loader"
        anchors.fill: parent
        source:backend.fragmentName//"Widget.qml"
        Component.onCompleted: {
            backend.setMainLoader(loader)
        }
    }
}
