import QtQuick 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4

Rectangle {
    id:rect
    property bool requered: false
    property bool checkedPlugin: false
    property string pluginName: "Default name"
    property int itemWidth: 200
    signal checkChanged(bool check)

    radius: 3
    border.color: "#040405"
    color: checkedPlugin ? "#2DA192" :"#303338"
    height: 46

    RHCheckBox {
        checkBoxWidth: itemWidth
        checkBoxColor: checkedPlugin ? "#2DA192":"#303338"
        checkBoxText: pluginName
        isToogled: checkedPlugin
        onCheckChanged: rect.checkChanged(checked)
    }
}
