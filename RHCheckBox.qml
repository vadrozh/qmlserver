import QtQuick 2.2

Rectangle {
    //body
    id: checkBox
    property bool isToogled: false
    property int checkBoxWidth: 200
    property int checkBoxHeight: 50
    property int checkBoxBorderRadius: 3
    property string checkBoxColor: isToogled ? "#2DA192" :"transparent"
    property string checkBoxBorderColor: isToogled ? "#2DA192" :"#040405"
    property string checkBoxText: "DEFAULT"
    signal checkChanged(bool checked)

    width: checkBoxWidth
    height: checkBoxHeight
    color: checkBoxColor
    radius: checkBoxBorderRadius
    border.color: checkBoxBorderColor

    Row {
        spacing: 10
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.topMargin: (checkBoxHeight - 22) / 2
        //Icon rectangle
        Rectangle {
            id:iconRectangle
            width: 22
            height: 22
            radius: 2
            color: checkBox.isToogled ? "#ffffff":"transparent"
            border.color: "#ffffff"
            Text {
                anchors.centerIn: parent
                id: iconText
                color: "#2DA192"
                font.pixelSize: 16
                text: checkBox.isToogled ? "\uf00c" : " "
            }
        }
        Text {
            color:"#ffffff"
            font.pixelSize: 16
            text: checkBoxText
            height: 22
            verticalAlignment: Qt.AlignVCenter
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            if(checkBox.isToogled) {
                checkBoxBorderColor = "#040405"
                checkBoxColor = "#303338"
                iconRectangle.color = "transparent"
                checkBox.isToogled = false
                checkBox.checkChanged(false)
            } else {
                checkBoxBorderColor = "#2DA192"
                checkBoxColor = "#2DA192"
                iconRectangle.color = "#ffffff"
                checkBox.isToogled = true
                checkBox.checkChanged(true)
            }
        }
    }
}
