import QtQuick 2.0

Rectangle {
    id: rect
    property bool commonBorder : true
    property int leftBorderWidth : 1
    property int rightBorderWidth : 1
    property int topBorderWidth : 1
    property int bottomBorderWidth : 1
    property int commonBorderWidth : 1
    property int borderRadius: 0

    z : -1

    property string borderDefaultColor: "white"
    property string borderHoveredColor: "white"
    property string borderFocusedColor: "white"
    property string borderCurrentColor: "white"

    color: borderCurrentColor
    radius: borderRadius

    anchors {
        left: parent.left
        right: parent.right
        top: parent.top
        bottom: parent.bottom

        topMargin    : commonBorder ? -commonBorderWidth : -topBorderWidth
        bottomMargin : commonBorder ? -commonBorderWidth : -bottomBorderWidth
        leftMargin   : commonBorder ? -commonBorderWidth : -leftBorderWidth
        rightMargin  : commonBorder ? -commonBorderWidth : -rightBorderWidth
    }

    function checkBorders(hovered, focused) {
        if(focused) {
            rect.color = rect.borderFocusedColor
            return rect.borderFocusedColor
        }
        else if(hovered) {
            rect.color = rect.borderHoveredColor
            return rect.borderHoveredColor
        }
        else {
           rect.color = rect.borderCurrentColor
           return rect.borderHoveredColor
        }
    }
}
