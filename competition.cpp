#include <QQmlContext>

#include "competition.h"




Competition::Competition(QQmlApplicationEngine *engine, int port, QObject *parent) : QObject(parent) {
    m_Info = new competitionInfo_t();
    m_Engine = engine;
    m_Server = new TcpServer();
    m_Server->listen(port);
    connect(m_Server, &TcpServer::clientConnected, this, [=](){emit refereeConnected();});
    connect(m_Server, &TcpServer::clientDisconnected, this, [=](){emit refereeDisconnected();});
}

void Competition::setName(QString name) {
    m_Info->setName(name);
}

void Competition::setPlugin(QObject *plugin) {
    if(CountingSystem* myPlugin = qobject_cast<CountingSystem*>(plugin)){
        m_Plugin = myPlugin;
        connect(m_Server, &TcpServer::newConnection, m_Plugin, &CountingSystem::refereeConnected);
    }
}


competitionInfo_t *Competition::getCompetitionInfo() {
    return m_Info;
}

QQmlComponent *Competition::getCompetitionWidget() {
        QString pluginPath = m_Plugin->getComponentPath();
        m_Component = new QQmlComponent(m_Engine,":/Widget.qml");
      //  QQmlContext *context = new QQmlContext(m_Engine->rootContext());
        QObject *object = m_Plugin->getContext();
      //  context->setContextObject(myPlugin->getContext());
        m_Engine->rootContext()->setContextProperty(m_Plugin->getContextName(), object);
        m_Component->create();


    return m_Component;
}


competitionInfo_t::competitionInfo_t(QObject *parent) : QObject(parent){

}

void competitionInfo_t::setName(QString name) {
    m_Name = name;
    emit nameChanged();
}

QString competitionInfo_t::getName() {
    return m_Name;
}



