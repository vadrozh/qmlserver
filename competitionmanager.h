#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

#include "competition.h"
#include "countingsystem.h"

class CompetitionManager : public QObject {
    Q_OBJECT
public:
    explicit                CompetitionManager(QQmlApplicationEngine* engine = nullptr, QObject *parent = nullptr);
    QQmlComponent*          createCompetition(QString competitionName, CountingSystem* plugin);
    QList<QObject *>        getCompetitionsInfo();
private:
    QList<Competition*>     m_Competitions;
    QList<QObject*>         m_CompetitionsInfo;
    QQmlApplicationEngine*  m_Engine;
    int                     m_CompetitionPort;
signals:
    void                    clientConnected();
    void                    clientDisconnected();

};


