import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    id:rect
    anchors.fill: parent
    color: "#36393E"
    Popup{
        id:popup2
        x: (rect.width - width) / 2
        y: (rect.height - height) / 2
        width: 620
        height: 470
        background: Rectangle{
            radius: 3
       }
        contentItem:
            Label{
                text: "ДОСТУПНЫЕ ПЛАГИНЫ"
                color:"#2DA192"
                anchors.left: parent.left
                anchors.topMargin: 30
                anchors.top: parent.top
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 22
                anchors.right: parent.right
            }

            GridView{
                ListModel {
                    id:gridModel
                    ListElement {
                    }
                    ListElement {
                    }
                    ListElement {
                    }
                    ListElement {
                    }
                }
                anchors.fill: parent
                anchors.margins: 35
                anchors.topMargin: 50
                cellWidth: 110
                cellHeight: 110
                clip: true
                Component{
                    id:pluginDelegate
                    Rectangle{
                        id: pluginDelegateRect
                        radius: 3
                        border.color:"#F3F3F3"
                        border.width: 2
                        height: 100
                        width: 100
                        anchors.margins: 10
                        Text{
                            id: pluginDelegateText
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            wrapMode: Text.Wrap
                            font.pixelSize: 16
                        //    anchors.left: parent.left
                        //    anchors.right: parent.right
                        //    anchors.leftMargin: 10
                         //   anchors.rightMargin: 10
                            text:model.modelData.name
                            color:"#99AAB5"
                        }
                        MouseArea{
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: {
                                pluginDelegateRect.border.color = "#2DA192"
                            }
                            onExited: {
                                pluginDelegateRect.border.color = "#F3F3F3"
                            }
                            onClicked: {
                                popup2.close()
                                pluginNameLabel.text = pluginDelegateText.text
                                pluginNameLabel.color = "#949393"
                            }
                        }
                    }
                }
                model: competitionPluginsModel
                delegate: pluginDelegate
            }
    }

    Popup {
        id: popup
        x: (rect.width - width) / 2
        y: (rect.height - height) / 2
        width: 520
        height: 420
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        background: Rectangle{
            radius: 3
       }
        contentItem: Rectangle{
            radius: 3
            Label {
                id:title
                text: "НОВОЕ СОРЕВНОВАНИЕ"
                font.pixelSize: 22
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                color:"#2DA192"
            }
            Label{
                id:compNameText
                text: "НАЗВАНИЕ СОРЕВНОВАНИЯ"
                anchors.top: title.bottom
                anchors.left: parent.left
                anchors.topMargin: 40
                anchors.leftMargin: 10
                font.pixelSize: 12
                color: "#6c6c6d"
            }

            TextField{
                id:control
                property string placeText: "Введите название сервера"
                anchors.top: compNameText.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.topMargin: 10
                font.pixelSize: 12
                placeholderText: "Введите название соревнования"
                background:
                    Rectangle {
                        anchors.fill: parent
                        implicitHeight: 40
                        implicitWidth: 350
                        color:"white"

                        CustomBorder {
                              id: rectBorder
                              commonBorder: false
                              borderRadius: 0
                              bottomBorderWidth: control.focus? 2 : 1
                              leftBorderWidth: 0
                              rightBorderWidth: 0
                              topBorderWidth: 0
                              borderCurrentColor: "#CDCDCD"
                              borderDefaultColor: checkBorders(control.hovered, control.focus)
                              borderHoveredColor: "#CDCDCD"
                              borderFocusedColor: "#2DA192"
                          }
                    }
            }
            Label{
                id:pluginNameText
                text: "ПЛАГИН СОРЕВНОВАНИЯ"
                anchors.top: control.bottom
                anchors.left: parent.left
                anchors.topMargin: 20
                anchors.leftMargin: 10
                font.pixelSize: 12
                color: "#6c6c6d"
            }
            Rectangle {
                radius: 3
                anchors.top: pluginNameText.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                border.color: "#CDCDCD"
                height: 50
                width: 350
                Rectangle{
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    width: 100
                    border.color: "#CDCDCD"
                    radius: 3
                }
                Rectangle{
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    anchors.margins: 1
                    width: 249
                    z:1
                    radius: 3
                    Label{
                        id:pluginNameLabel
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: 250
                        text: "Отсутствует"
                        font.pixelSize: 14
                        color: "#949393"
                        z:0
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }



                Button {
                    id:openPluginBtn
                    text:"Выбрать"
                    anchors.fill: parent
                    font.pixelSize: 14
                    background: Rectangle {
                        radius: 3
                        id:buttonRect
                        color: "transparent"
                    }
                    contentItem: Text{
                        id:btnText
                        text: openPluginBtn.text
                        font: openPluginBtn.font

                        color:"#2DA192"
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                    }
                    onClicked: {
                        popup2.open()
                    }

                    onHoveredChanged: {
                        if(hovered){
                            buttonRect.color = "#32A898"
                            btnText.color = "white"
                        } else {
                            buttonRect.color = "transparent"
                            btnText.color = "#2DA192"
                        }
                    }
                }


            }
            Button {
                id:newCompBtn
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                text: "Создать"
                font.pixelSize: 16
                height: 40
                width: 100
                contentItem: Text{
                    text: newCompBtn.text
                    font: newCompBtn.font
                    color: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                background: Rectangle {
                    id:backRect
                    color:"#2DA192"
                    implicitWidth: 100
                    implicitHeight: 40
                    radius: 3
                }
                onHoveredChanged: {
                    if(newCompBtn.hovered){
                        backRect.color = "#32A898"
                    } else {
                       backRect.color = "#2DA192"
                    }
                }

                onPressed:{
                    backRect.color = "#42B8A8"
                }
                onReleased: {
                    backRect.color = "#2DA192"
                }
                onClicked: {
                    if(pluginNameLabel.text!=="Отсутствует"){
                    popup.close()
                    bodyRectComp.isActive = true
                    bodyRectComp.color = "#2DA192"
                    bodyRect.color = "#2F3136"
                    bodyRect.isActive = false
                    backend.createCompetition(control.text, pluginNameLabel.text)
                    backend.setRightFragment("CompetitionsFragment.qml")
                    } else {
                        pluginNameLabel.color = "red"
                    }
                }
            }
       }
    }
    Rectangle{
        id: toolBar
        width: 70
        anchors.top: parent.top
        anchors.left: parent.Left
        color: "#202225"
        height: parent.height
        Column {
            width: 70
            Rectangle {
                id: homeContainer
                height: 70
                width: parent.width
                color: "transparent"
                Rectangle {
                    property bool isActive: true
                    id: bodyRect
                    color: isActive ? "#2DA192":"#2F3136"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 25
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        font.family: "FontAwesome"
                        text: "\uF015"
                        color: "#ffffff"
                        font.pixelSize: 16
                    }
                    MouseArea{
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: {
                            if(!parent.isActive)
                                bodyRect.color = "#36393F"
                        }
                        onExited: {
                            if(!parent.isActive)
                                bodyRect.color = "#2F3136"
                        }
                        onClicked: {
                            loader.setSource("ServerMainFragment.qml")
                            parent.isActive = true;
                            bodyRectAdd.isActive = false
                            bodyRectComp.isActive = false
                            bodyRectComp.color = "#2F3136"
                            bodyRectAdd.color = "#2F3136"
                            parent.color = "#2DA192"
                        }
                    }
                }
                Rectangle {
                    color: "#2F3136"
                    height: 2
                    width: 30
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }
            Rectangle {
                id: compConatiner
                height: 70
                width: parent.width
                color: "transparent"
                Rectangle {
                    id: bodyRectComp
                    property bool isActive: false
                    color: "#2F3136"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 25
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        text: "\uf11e"
                        color: "#ffffff"
                        font.pixelSize: 16
                    }
                    MouseArea{
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: {
                            if(!parent.isActive)
                                bodyRectComp.color = "#36393F"
                        }
                        onExited: {
                            if(!parent.isActive)
                                bodyRectComp.color = "#2F3136"
                        }
                        onClicked: {
                            loader.setSource("CompetitionsFragment.qml")
                            parent.isActive = true;
                            parent.color = "#2DA192"
                            bodyRect.isActive = false
                            bodyRectAdd.isActive = false
                            bodyRect.color = "#2F3136"
                            bodyRectAdd.color = "#2F3136"
                        }
                    }
                }
            }
            Rectangle {
                id: addConatiner
                height: 70
                width: parent.width
                color: "transparent"
                Rectangle {
                    id: bodyRectAdd
                    property bool isActive: false
                    color: "#2F3136"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 25
                    width: radius * 2
                    height: radius * 2
                    Label {
                        anchors.centerIn: parent
                        text: "+"
                        color: "#ffffff"
                        font.pixelSize: 16
                    }
                    MouseArea{
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: {
                            if(!parent.isActive)
                                bodyRectAdd.color = "#36393F"
                        }
                        onExited: {
                            if(!parent.isActive)
                                bodyRectAdd.color = "#2F3136"
                        }
                        onClicked: {
                       //     parent.isActive = true
                       //     parent.color = "#2DA192"
                       //     bodyRect.isActive = false
                       //     bodyRectComp.isActive = false
                       //     bodyRect.color = "#2F3136"
                        //    bodyRectComp.color = "#2F3136"
                            popup.open()
                        }
                    }
                }
            }
        }
    }
    Loader {
        id:loader
        y: 0
        height: parent.height
        anchors.left: toolBar.right
        width: parent.width - 60
        source: backend.rightFragment
    }

}
