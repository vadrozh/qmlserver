import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    anchors.fill: parent
    color: "transparent"
    id:rect
    Rectangle {
        id: rectHead
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: "#36393F"
        height: 50
        CustomBorder {
            commonBorder: false
            borderCurrentColor: "#222427"
            borderDefaultColor: "#222427"
            topBorderWidth: 0
            leftBorderWidth: 0
            rightBorderWidth: 0
            bottomBorderWidth: 1
        }
        Label {
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: 15
            text:"Сервер"
            color: "#ffffff"
            font.pixelSize: 22
        }


    }
    Rectangle {
        anchors.top:rectHead.bottom
        anchors.topMargin: 1
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: "#36393E"
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        RowLayout {
            id:tabsContainer
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 80
            Rectangle {
                id: competitionCountItem
                color: "#2DA192"
                radius: 3
                width: 220
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.leftMargin: 15
                anchors.topMargin: 20

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    id: iconCompText
                    text: qsTr("\uF11d")
                    color:"#ffffff"
                    font.pixelSize: 18
                }

                Text {
                    id: compCountInfoText
                    text: qsTr("Соревнований:")
                    color:"#ffffff"
                    font.pixelSize: 18
                    anchors.left: iconCompText.right
                    anchors.leftMargin: 10
                    anchors.verticalCenter: iconCompText.verticalCenter
                }
                Text {
                    id: compCount
                    text: backend.competitionsCount
                    color:"#ffffff"
                    font.pixelSize: 18
                    anchors.left: compCountInfoText.right
                    anchors.verticalCenter: compCountInfoText.verticalCenter
                }
            }

            Rectangle {
                id: clientCountItem
                color: "#2DA192"
                radius: 3
                width: 220
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                 anchors.bottomMargin: 20
                anchors.topMargin: 20
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    id: servCompText
                    text: qsTr("\uF2c0")
                    color:"#ffffff"
                    font.pixelSize: 18
                }

                Text {
                    id: servCountInfoText
                    text: qsTr("Клиентов:")
                    color:"#ffffff"
                    font.pixelSize: 18
                    anchors.left: servCompText.right
                    anchors.leftMargin: 10
                    anchors.verticalCenter: servCompText.verticalCenter
                }
                Text {
                    id: servCount
                    text: backend.clientsCount
                    color:"#ffffff"
                    font.pixelSize: 18
                    anchors.left: servCountInfoText.right
                    anchors.verticalCenter: servCountInfoText.verticalCenter
                }
            }
            Rectangle{
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 1
                anchors.leftMargin: 15
                anchors.rightMargin: 20
                color: "#484B51"
            }

        }
        Text {
            id: ipTextInfo
            text: qsTr("Ip-адрес")
            font.pixelSize: 18
            color: "#B9BBBE"
            anchors.top: tabsContainer.bottom
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.topMargin: 10
        }
        Rectangle{
            id:ipTextRect
            anchors.top: ipTextInfo.bottom
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 20
            color:"#303338"
            radius: 3
            border.color: "#222427"
            height: 35
            Text {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
                font.pixelSize: 16
                id: ipText
                text: backend.ip
                color:"#ffffff"
            }
        }
        Rectangle{
            id:ipLine
            anchors.top: ipTextRect.bottom
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.right: parent.right
            height: 1
            anchors.leftMargin: 15
            anchors.rightMargin: 20
            color: "#484B51"
        }

        Text {
            id: portInfoText
            text: qsTr("Порт")
            anchors.top: ipLine.bottom
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 15
            font.pixelSize: 18
            color: "#B9BBBE"
        }
        Rectangle{
            id:portTextRect
            anchors.top: portInfoText.bottom
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.topMargin: 10
            color:"#303338"
            radius: 3
            border.color: "#222427"
            height: 35
            Text {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
                font.pixelSize: 16
                id: portText
                text: qsTr("22222")
                color:"#ffffff"
            }
        }
        Rectangle{
            id:portLine
            anchors.top: portTextRect.bottom
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.right: parent.right
            height: 1
            anchors.leftMargin: 15
            anchors.rightMargin: 20
            color: "#484B51"
        }
    }

}
