import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Rectangle {
    anchors.fill: parent
    color:"transparent"
    Label {
        id:welcomeLabel
        text:"Добро пожаловать на сервер"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 80
        font.pixelSize: 22
        color:"#ffffff"
    }
    Rectangle{
        id: createHelper
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: welcomeLabel.bottom
        width:320
        height: 60
        anchors.topMargin: 40
        color:"transparent"
        Label {
            id: iconPlusLabel
            text: "\uf11d"
            font.pixelSize: 22
            color:"#ffffff"
            anchors.left: parent
            anchors.top: parent.top
        }

            Label {
                id: plusLabel
                text: "Создайте новое соревнование"
                font.pixelSize: 14
                font.bold: true
                color:"#B9BBBE"
                anchors.left: iconPlusLabel.right
                anchors.leftMargin: 5
                anchors.top: parent.top
            }
            Label {
                id:plusText
            }

    }
}
