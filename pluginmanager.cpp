#include <QPluginLoader>
#include <QDebug>
#include <QDir>

#include "pluginmanager.h"

PluginManager::PluginManager(QObject *parent) : QObject(parent) {
    m_PluginsDir = QDir(QDir::currentPath() + "/Plugins");
    qDebug() << this << "Loaded";
}

PluginManager::~PluginManager() {
    foreach (QObject* plugin, m_Plugins) {
        delete plugin;
    }
}

QList<QObject*> PluginManager::getPluginsInfo(){
 /*   foreach (QObject* plugin, m_Plugins) {
        if(RhPlugin* jsPlugin = qobject_cast<RhPlugin*>(plugin)) {
            m_PluginsInfo.append(jsPlugin->getInfo());
        } else {
            CountingSystem* cPlugin = qobject_cast<CountingSystem*>(plugin);
            rhPluginInfo_t* info = new rhPluginInfo_t();
            info->setOwner(cPlugin);
            info->setPluginName(cPlugin->getName());
            info->setChecked(false);
            m_PluginsInfo.append(info);
        }
    }*/
    return m_PluginsInfo;
}

QList<QObject *> PluginManager::getCheckedCompetitionPlugins() {
    checkPlugins();
    return m_PluginsInfo;
}

void PluginManager::checkPlugins() {
    foreach (QObject* info, m_PluginsInfo) {
        rhPluginInfo_t* pluginInfo = qobject_cast<rhPluginInfo_t*>(info);
        if(!pluginInfo->getChecked()){
            m_PluginsInfo.removeOne(pluginInfo);
        } else {
            pluginInfo->setOwner(loadPlugin(pluginInfo->getPluginPath()));
        }
    }
}

CountingSystem *PluginManager::loadPlugin(QString path) {
    QPluginLoader loader(path);
    if(loader.load()) {
        if(CountingSystem* myPlugin = qobject_cast<CountingSystem*>(loader.instance())){
         //  m myPlugin->getContext();
           return myPlugin;
        }
    }
}

/*QList<QObject *> PluginManager::getCheckedCompetitionPlugins() {
    checkPlugins();
    return m_PluginsInfo;
}*/

/*QList<QObject *> PluginManager::getCheckedSystemPlugins() {
    return m_PluginsInfo;
}*/

CountingSystem *PluginManager::getCompetitionPlugin(QString pluginName) {
    foreach (QObject* info, m_PluginsInfo) {
        rhPluginInfo_t* pluginInfo = qobject_cast<rhPluginInfo_t*>(info);
        if(pluginInfo->getPluginName() == pluginName){
            return qobject_cast<CountingSystem*>(pluginInfo->getOwner());
        }
    }
    return NULL;
}

void PluginManager::loadPlugins(QDir dir) {
    QFileInfoList files = dir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files|QDir::Dirs, QDir::DirsFirst);
    foreach (const QFileInfo &file, files){
        if(file.isDir()){
            loadPlugins(QDir(file.filePath()));
            continue;
        }
        if(file.isFile() && file.completeSuffix()=="rh.js"){
         /*   RhPlugin *plugin = new RhPlugin();
            if(!plugin->loadPlugin(file.absoluteFilePath())){

            } else {
                m_Plugins.append(plugin);
            }*/
        } else {
            QPluginLoader loader(file.absoluteFilePath());
            if(loader.load()) {
                if(CountingSystem* myPlugin = qobject_cast<CountingSystem*>(loader.instance())){
                    rhPluginInfo_t* info = new rhPluginInfo_t;
                    info->setPluginName(myPlugin->getName());
                    info->setPluginPath(file.absoluteFilePath());
                    QString name = myPlugin->getName();
                    qDebug() << name;
                    m_PluginsInfo.append(info);
                }
                loader.unload();
            } else {
                qDebug() << "Failed to load: " << loader.errorString();
            }
        }
    }
}
