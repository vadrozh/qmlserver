#include "competitionmanager.h"

CompetitionManager::CompetitionManager(QQmlApplicationEngine* engine, QObject *parent) : QObject(parent) {
    m_Engine = engine;
    m_CompetitionPort = 50100;
}

QQmlComponent* CompetitionManager::createCompetition(QString competitionName, CountingSystem* plugin){
    Competition *competiton = new Competition(m_Engine, m_CompetitionPort);
    competiton->setName(competitionName);
    competiton->setPlugin(plugin);
    connect(competiton, &Competition::refereeConnected, this, [=](){emit clientConnected();});
    connect(competiton, &Competition::refereeDisconnected, this, [=](){emit clientDisconnected();});

    m_Competitions.append(competiton);
    m_CompetitionsInfo.append(competiton->getCompetitionInfo());
    m_CompetitionPort++;
    return competiton->getCompetitionWidget();
}

QList<QObject *> CompetitionManager::getCompetitionsInfo() {
    return m_CompetitionsInfo;
}
