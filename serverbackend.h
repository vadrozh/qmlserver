#pragma once
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QMap>

#include "pluginmanager.h"
#include "competitionmanager.h"
#include "tcpserver.h"

class ServerBackend : public QObject {
    Q_OBJECT
public:
    explicit                ServerBackend(QObject *parent = nullptr, QQmlApplicationEngine* engine = nullptr);
    Q_INVOKABLE QString     getFragmentName(int fragmentNum);
    Q_INVOKABLE QString     getFragment();
    Q_INVOKABLE QString     getCurrentPort();
    Q_INVOKABLE QString     getRightFragment();
    Q_INVOKABLE QString     getIp();
    Q_INVOKABLE QString     getPort();
    Q_INVOKABLE QString     getClientsCount();
    Q_INVOKABLE QString     getCompetitionsCount();
   // Q_INVOKABLE QString     getPluginFragment();
    Q_INVOKABLE QQmlComponent* getPluginComponent();
    Q_INVOKABLE void        setCurrentPort(QString port);
    Q_INVOKABLE void        setCompetitionsCount(QString competitionsCount);
    Q_INVOKABLE void        setClientsCount(QString clientsCount);
    Q_INVOKABLE void        setPort(QString port);
    Q_INVOKABLE void        setIp(QString ip);
    Q_INVOKABLE void        openPluginComponent(QString component);
    Q_INVOKABLE void        setFragment(QString name);
    Q_INVOKABLE void        setMainLoader(QObject* loader);
    Q_INVOKABLE void        setRightFragment(QString fragmentName);
    Q_INVOKABLE void        endInitialize();
    Q_INVOKABLE void        createCompetition(QString competitionName, QString pluginName);
    Q_INVOKABLE void        openCompetitionWidget(QString name);
    Q_INVOKABLE void        setPluginComponent(QQmlComponent* component);
public:
    Q_PROPERTY(QString currentPort READ getCurrentPort WRITE setCurrentPort NOTIFY currentPortChanged)
    Q_PROPERTY(QString competitionsCount READ getCompetitionsCount WRITE setCompetitionsCount NOTIFY competitionsCountChanged)
    Q_PROPERTY(QString clientsCount READ getClientsCount WRITE setClientsCount NOTIFY clientsCountChanged)
    Q_PROPERTY(QString ip READ getIp WRITE setIp NOTIFY ipChanged)
    Q_PROPERTY(QQmlComponent* pluginComponent READ getPluginComponent WRITE setPluginComponent NOTIFY pluginComponentChanged)
    Q_PROPERTY(QString fragmentName READ getFragment WRITE setFragment NOTIFY fragmentChanged)
   // Q_PROPERTY(QString pluginFragment READ getPluginFragment WRITE setPluginFragment NOTIFY pluginFragmentChanged)
    Q_PROPERTY(QString rightFragment READ getRightFragment WRITE setRightFragment NOTIFY rightFragmentChanged)
    Q_PROPERTY(QList<QObject*> competitionList READ getCompetitionList NOTIFY competitionListChanged)
    Q_PROPERTY(QString port READ getPort WRITE setPort NOTIFY portChanged)
public slots:
    void                    refereeConnected();
    void                    refereeDisconnected();
private:
    int                     m_CompetitionsCount;
    int                     m_CompetitionPort;
    int                     m_Clients;
    QString                 m_CurrentCompetitionPort;
    QQmlApplicationEngine*  m_Engine;
    PluginManager*          m_PluginManager;
    CompetitionManager*     m_CompetitionManager;
    QObject*                m_MainLoader;
    TcpServer*              m_MainServer;
    QString                 m_ClientsCount;
    QString                 m_Ip;
    QString                 m_Port;
    QString                 m_CurrentFragmentName;
    QString                 m_RightFragmentName;
    QString                 m_PluginFragmentName;
    QQmlComponent           m_PluginFragmentComponent;
    QMap<QString, QQmlComponent*> m_PlugnsComponents;
    QQmlComponent*          m_CurrentComponent;
signals:
//    void                    pluginFragmentChanged();
    void                    competitionsCountChanged();
    void                    clientsCountChanged();
    void                    ipChanged();
    void                    fragmentChanged();
    void                    competitionListChanged();
    void                    rightFragmentChanged();
    void                    portChanged();
    void                    pluginComponentChanged();
    void                    currentPortChanged();
public slots:
    void                    registerPluginsData();
    void                    registerCompetitionData();
    QList<QObject*>         getCompetitionList();
};

