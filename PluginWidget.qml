import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Controls 1.4 as C
import QtQuick.Layouts 1.3

Rectangle{
    id:widget
    width: 500//parent.width
    height: 500// parent.height
    color:"gray"
    ListModel {
        id: libraryModel
        ListElement {
            title: "A Masterpiece"
            author: "Gabriel"
        }
        ListElement {
            title: "Brilliance"
            author: "Jens"
        }
        ListElement {
            title: "Outstanding"
            author: "Frederik"
        }
    }

    C.TableView{
        width: 393
        height: 292
        C.TableViewColumn {
                role: "title"
                title: "Title"
                width: 100
            }
            C.TableViewColumn {
                role: "author"
                title: "Author"
                width: 200
            }
            model: libraryModel
    }

}
