#pragma once

#include <QObject>
#include <QList>
#include <QDir>

#include "rhplugin.h"
#include "countingsystem.h"

class PluginManager : public QObject {
    Q_OBJECT
public:
    explicit                PluginManager(QObject *parent = nullptr);
                            ~PluginManager();
    QList<QObject*>         getPluginsInfo();
    QList<QObject*>         getCheckedCompetitionPlugins();
    CountingSystem*         getCompetitionPlugin(QString pluginName);

    void                    loadPlugins(QDir dir);
    void                    loadPluginsInfo();
    void                    checkPlugins();
private:
    CountingSystem*         loadPlugin(QString path);
private:
    QList<QObject*>         m_PluginsInfo;
    QList<QObject*>         m_Plugins;
    QList<CountingSystem*>  m_LoadedPlugins;
    QDir                    m_PluginsDir;
};
