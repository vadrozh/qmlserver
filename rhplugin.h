#ifndef RHPLUGIN_H
#define RHPLUGIN_H

#include <QObject>
#include<QJSEngine>
#include<QScopedPointer>

class RhPlugin;

struct rhPluginInfo_t : public QObject{
    explicit                rhPluginInfo_t(QObject* parent = nullptr);
public slots:
    QString                 getPluginName();
    QObject*                getOwner();
    bool                    getChecked();
    QString                 getPluginPath();

    void                    setPluginPath(QString path);
    void                    setChecked(bool checked);
    void                    setOwner(QObject* owner);
    void                    setPluginName(QString name);
signals:
    void                    nameChanged();
    void                    checkChanged();
private:
    Q_OBJECT
    Q_PROPERTY(QString name READ getPluginName WRITE setPluginName NOTIFY nameChanged)
    Q_PROPERTY(bool checked READ getChecked WRITE setChecked NOTIFY checkChanged)

  //  RhPlugin*               m_Plugin;
    QObject*                m_Plugin;
    QString                 m_PluginDescription;
    QString                 m_PluginName;
    QString                 m_Path;
    bool                    m_Checked;
};

class RhPlugin : public QObject
{
    Q_OBJECT
public:
    explicit                RhPlugin(QObject *parent = nullptr);
                            ~RhPlugin();
    bool                    loadPlugin(QString path);
    void                    installJsContext(QJSEngine* engine);
    rhPluginInfo_t*         getInfo();
signals:

public slots:
    void                    test();
private:
    QJSEngine*              m_Engine;
    rhPluginInfo_t*         m_Info;
};

#endif // RHPLUGIN_H
