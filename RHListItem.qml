import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3



Rectangle {
    id: rect

    property int itemWidth: 225
    property int itemHeight: 32
    property int borderRadius: 3
    property string itemText: "DEFAULT"
    property string defaultColor: "transparent"
    property string hoveredColor: "#36393F"
    property string activeColor: "#42464D"
    property string defaultTextColor: "#878889"
    property string hoveredTextColor: "#939394"
    property string activeTextColor: "#ffffff"
    property bool selected: false
    signal clicked(bool selection)

    width: itemWidth
    height: itemHeight
    color: selected ? activeColor : defaultColor
    radius: borderRadius

    MouseArea{
        hoverEnabled: true
        anchors.fill: parent
        onClicked: {
         //   if(!rect.selected){
              //  rect.color = rect.activeColor
             //   itemLabel.color = rect.activeTextColor
//                rect.selected = true
                rect.clicked(true)

          //  }
        }
        onEntered: {
            if(!rect.selected){
                rect.color = rect.hoveredColor
                itemLabel.color = rect.hoveredTextColor
            }
        }
        onExited: {
            if(!rect.selected){
                rect.color = rect.defaultColor
                itemLabel.color = rect.defaultTextColor
            }
        }
    }

    Label {
        id:itemLabel
        text: rect.itemText
        font.pixelSize: 14
        color: rect.selected ? rect.activeTextColor:rect.defaultTextColor
        anchors.left: rect.left
        anchors.leftMargin: 20
        anchors.verticalCenter: rect.verticalCenter
    }

}
