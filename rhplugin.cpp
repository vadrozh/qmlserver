#include "rhplugin.h"

#include <QFile>
#include <QTextStream>
#include <QJSValue>
#include <QDebug>

RhPlugin::RhPlugin(QObject *parent) : QObject(parent) {

}

RhPlugin::~RhPlugin() {
    delete m_Info;
    delete m_Engine;
}
bool RhPlugin::loadPlugin(QString path) {
    QFile scriptFile(path);
    if(!scriptFile.open(QIODevice::ReadOnly)){

    }
    m_Info = new rhPluginInfo_t;
    m_Info->setOwner(this);
    m_Engine = new QJSEngine();
    QTextStream stream(&scriptFile);
    QString content = stream.readAll();
    scriptFile.close();
    m_Engine->evaluate(content, path);
    QJSValue res = m_Engine->evaluate("name");
    QString name = res.toString();
    m_Info->setPluginName(name);
    m_Info->setChecked(false);
    res = m_Engine->evaluate("desk");
    QString desk = res.toString();
    QJSValue globalObject = m_Engine->globalObject();
    globalObject.setProperty("rhApi", m_Engine->toScriptValue(static_cast<QObject*>(this)));

    QJSValue s = m_Engine->evaluate("test");
    if(!s.isCallable()){

    }
    s = s.call();
    return true;
}

void RhPlugin::installJsContext(QJSEngine *engine) {
    QJSValue globalObject = engine->globalObject();
}

void RhPlugin::test() {
    qDebug() << "Test";
}


rhPluginInfo_t* RhPlugin::getInfo() {
    return m_Info;
}

rhPluginInfo_t::rhPluginInfo_t(QObject *parent) : QObject(parent){
    m_Checked = false;
}

void rhPluginInfo_t::setPluginName(QString name){
    m_PluginName = name;
}

void rhPluginInfo_t::setChecked(bool checked){
    m_Checked = checked;
}

void rhPluginInfo_t::setOwner(QObject *owner){
    m_Plugin = owner;
}

QObject *rhPluginInfo_t::getOwner(){
    return m_Plugin;
}

QString rhPluginInfo_t::getPluginName(){
    return m_PluginName;
}

bool rhPluginInfo_t::getChecked(){
    return m_Checked;
}

QString rhPluginInfo_t::getPluginPath() {
    return m_Path;
}

void rhPluginInfo_t::setPluginPath(QString path) {
    m_Path = path;
}
